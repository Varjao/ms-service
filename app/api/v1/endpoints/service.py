from typing import List, Optional

import httpx
from bson import ObjectId
from fastapi import APIRouter, Depends, Query

from app.db.mongodb import AsyncIOMotorClient
from app.middleware.db import get_db
from app.schema.service import ServiceBase, ServiceInDB

router = APIRouter()

@router.get('/')
async def get_services(
    db: AsyncIOMotorClient = Depends(get_db),
    limit: int = Query(10, gt=0),
    skip: int = Query(0, ge=0),
) -> List[ServiceInDB]:
    
    services: List[ServiceInDB] = []

    rows = db['1help']['services'].find(limit=limit, skip=skip)
    services = [ ServiceInDB(**row) async for row in rows ]
    
    return services

@router.get('/{id}')
async def get_service(
    db: AsyncIOMotorClient = Depends(get_db),
    *,
    id: str
) -> ServiceInDB:
    service = await db['1help']['services'].find_one({"user_id":ObjectId(id) })
    return ServiceInDB(**service)

@router.post('/')
async def create_service(
    db: AsyncIOMotorClient = Depends(get_db),
    *,
    service_data: ServiceBase
) -> Optional[ServiceInDB]:
    
    service = service_data.dict()
    # mando pro servico de maximizar o disconto pelos cupons
    print(service)
    async with httpx.Client() as client:
        discount = await client.post( url='http://localhost:8080/discount', 
        json = 
        {
            "user_id": str(service['user_id']),
            "service_value": service['value']
        } 
    )
    if discount.json().get('error') :
        return discount.json()
    # atualizo os valores do servico
    discount = discount.json()
    service['discount'] = discount.get('discount')
    service['coupon']  = discount.get('coupon', None)
    service['value_with_discount'] = discount.get('value_with_discount', None)
    # salvo o servico no banco
    await db['1help']['services'].insert_one(service)
    service['_id']
    return ServiceInDB(**service)
    