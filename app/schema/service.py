from typing import List, Optional

from datetime import datetime
from pydantic import Field, root_validator

from app.schema.base import DBModelMixin, ObjectIdStr
from app.schema.rwmodel import RWModel


class ServiceBase(RWModel):
    user_id: ObjectIdStr = Field(..., alias="userId")
    day: datetime
    duration: int = Field(..., gt=0, lt=12) 
    value: int = Field(..., gt=0)           
    

class ServiceInDB(DBModelMixin, ServiceBase):
    discount: int = Field(..., ge=0)
    coupon: Optional[str]
    value_with_discount: int
    
    pass